import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NodesService } from 'src/services/Nodes.service';

@Component({
  selector: 'app-operations',
  templateUrl: './operations.component.html',
  styleUrls: ['./operations.component.css']
})
export class OperationsComponent implements OnInit {

  input: string = null;

  operations: string[] = [
    'AND', 'OR', 'NOT', 'LIKE', 'WHERE',
    '=', '>', '<', '< >', '>=', '<=',
    'AVG', 'MIN', 'MAX',
    '(', '+', '-', '*', '/', ')'
  ];

  @Output() emitClick = new EventEmitter<string>();

  constructor(private nodesService: NodesService) { }

  ngOnInit() { }

  onDoubleClick(event: Event): void {
    const selectedTransformation: string = (<HTMLDivElement>event.target).childNodes[0].textContent
    const nodeType = selectedTransformation.replace(/ /g,'')
    this.emitClick.emit(nodeType)
  }

  onEnterInput(event : Event) {
    if(this.input != null)
      if(!this.input.match(' ')){
        this.emitClick.emit(this.input)
        this.input = null
      }

  }

}


