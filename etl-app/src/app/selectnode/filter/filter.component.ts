import { Component, OnInit, Input } from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { SelectNode } from 'src/models/SelectNode';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

  @Input() addedOperationName: string;
  @Input() node: SelectNode;

  operations: string[] = [];

  constructor() {  }

  ngOnInit() {
    this.operations = this.node.operations
  }
  
  ngOnChanges() {
    this.operations = this.node.operations
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.operations, event.previousIndex, event.currentIndex);
  }

}
