import { Component, OnInit, Input } from '@angular/core';
import { SelectNode } from 'src/models/SelectNode';
import { OpsService } from 'src/services/Ops.service';
import { FileNode } from 'src/models/FileNode';
import { NodesService } from 'src/services/Nodes.service';

@Component({
  selector: 'app-columns',
  templateUrl: './columns.component.html',
  styleUrls: ['./columns.component.css']
})
export class ColumnsComponent implements OnInit {

  @Input() node: SelectNode;

  columns:string[] = [];

  constructor(private opService: OpsService, private nodeService:NodesService) { }

  ngOnInit() {
    const g = this.opService.graph;
    const a = g.nodeEdges(this.node.id)
    if(a){
      const source = this.nodeService.getNode(a[0].v)
      if(source && (<FileNode>source).file)
        this.columns = (<FileNode>source).file.columns
    }

  }

  checkValue(checkedColumn: any){
 
      const index = this.node.columns.indexOf(checkedColumn)
      if(index != -1)
        this.node.columns.splice(index,1)
      else
        this.node.columns.push(checkedColumn)
  
      console.log(this.node.columns)

  }

}
