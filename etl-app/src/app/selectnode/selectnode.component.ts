import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NodesService } from 'src/services/Nodes.service';
import { SelectNode } from 'src/models/SelectNode';

@Component({
  selector: 'app-selectnode',
  templateUrl: './selectnode.component.html',
  styleUrls: ['./selectnode.component.css']
})
export class SelectnodeComponent implements OnInit {

  constructor(private route: ActivatedRoute, private nodesService: NodesService) { }

  nodeId:number;
  addedOperationName: String;
  node : SelectNode;
  

  ngOnInit() {
    this.nodeId = this.route.snapshot.params['id']
    this.node = <SelectNode>(this.nodesService.getNode(this.nodeId))
  }

  addedOperation(addedOperationName: string){
    this.addedOperationName = new String(addedOperationName)
    this.node.addOperation(addedOperationName)
  }

}
