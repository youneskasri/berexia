import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DragDropModule } from '@angular/cdk/drag-drop'
import { RouterModule, Routes } from '@angular/router';
import { NgxGraphModule } from '@swimlane/ngx-graph';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FileDropModule } from 'ngx-file-drop';
import { HttpModule } from '@angular/http'
import { AgGridModule } from 'ag-grid-angular';
import { FormsModule} from '@angular/forms';


import { AppComponent } from './app.component';
import { TransformationsComponent} from './main/transformations/transformations.component';
import { FlowchartComponent } from './main/flowchart/flowchart.component';

import { SelectnodeComponent } from './selectnode/selectnode.component';
import { FilterComponent } from './selectnode/filter/filter.component';
import { MainComponent } from './main/main.component'
import { ColumnsComponent } from './selectnode/columns/columns.component';
import { OperationsComponent } from './selectnode/operations/operations.component';
import { NodesService } from 'src/services/Nodes.service';
import { DatasetsComponent } from './main/datasets/datasets.component';
import { FilesService } from 'src/services/Files.service';
import { OpsService } from 'src/services/Ops.service';
import { PreviewComponent } from './preview/preview.component';

const appRoutes: Routes =  [
  { path: 'selectnode/:id', component: SelectnodeComponent },
  { path: 'main', component: MainComponent },
  { path: 'preview', component: PreviewComponent},
  { path: '',
    redirectTo: '/main',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    TransformationsComponent,
    FlowchartComponent,
    SelectnodeComponent,
    FilterComponent,
    ColumnsComponent,
    OperationsComponent,
    MainComponent,
    DatasetsComponent,
    PreviewComponent

  ],
  imports: [
    RouterModule.forRoot(
      appRoutes
      
    ),
    BrowserModule,
    FormsModule,
    DragDropModule,
    NgxGraphModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    FileDropModule,
    HttpModule,
    AgGridModule.withComponents([])
  ],
  providers: [NodesService, FilesService, OpsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
