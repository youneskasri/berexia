import { Component, OnInit } from '@angular/core';
import { OpsService } from 'src/services/Ops.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  addedChart: String;
  addedFile: any;

  constructor(private opService:OpsService) { }

  ngOnInit() {
  }

  childTransfEvent(event : string){
    this.addedChart = new String(event);
  }

  childFilesEvent(event : any){
    this.addedFile = event;
  }

  run(){
    console.log('ruuuning')
    this.opService.execute()
  }

}


