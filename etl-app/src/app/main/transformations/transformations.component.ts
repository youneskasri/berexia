import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';


@Component({
  selector: 'app-transformations',
  templateUrl: './transformations.component.html',
  styleUrls: ['./transformations.component.css']
})
export class TransformationsComponent implements OnInit {

  transformations: string[] = [
    'Select',
    'Combine',
    'GroupBy'
  ]

  @Output() emitClick = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {

  }

  onDoubleClick(event:Event) : void{
    const selectedTransformation: string = (<HTMLDivElement>event.target).childNodes[0].textContent
    const nodeType = selectedTransformation.replace(/ /g,'')
    this.emitClick.emit(nodeType)
  }


}
