import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { UploadEvent, UploadFile, FileSystemFileEntry } from 'ngx-file-drop';
import { FilesService } from 'src/services/Files.service';
import { MFile } from 'src/models/MFile';

import { SERVER_BASE_URL } from "../../../params";

@Component({
  selector: 'app-datasets',
  templateUrl: './datasets.component.html',
  styleUrls: ['./datasets.component.css']
})
export class DatasetsComponent implements OnInit {

  uploadedFiles: UploadFile[] = [];
  files: MFile[] = [];

  @Output() emitClick = new EventEmitter<MFile>();

  constructor(private fileService: FilesService) { 
  }

  ngOnInit() { }
 
  dropped(event: UploadEvent) {

    this.uploadedFiles = event.files;
    for (const droppedFile of event.files) {
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          const reader = new FileReader();
          reader.addEventListener("load",() => {

            const content = <string>reader.result
            const name = droppedFile.relativePath;
            const fichier = new MFile(name,content)
            this.files.push(fichier)
            
            /*REPLACE URL with your route*/ 
            this.fileService.uploadFileToServer(`${SERVER_BASE_URL}/files`,fichier)
              .subscribe(resp=>{
                fichier.table_name = resp.tableName;
                fichier.columns = resp.columnsList;
                alert('Upload done !')
              },err=>{
                console.log('upload failed',err);
              })
            /**/ 

          }, false)
        reader.readAsDataURL(file)
        });
      }
    }
  }

  onDoubleClick(file:MFile) : void{
    this.emitClick.emit(file)
    console.log('emitted',file)
  }

  fileOver(event){
    console.log(event);
  }
 
  fileLeave(event){
    console.log(event);
  }

}
