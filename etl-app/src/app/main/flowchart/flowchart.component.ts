import { Component, OnInit, Input } from '@angular/core';
import { Node } from '../../../models/Node'
import { Router } from '@angular/router';
import { NodesService } from 'src/services/Nodes.service';

import * as shape from 'd3-shape';
import { MFile } from 'src/models/MFile';
import { FilesService } from 'src/services/Files.service';
import { FileNode } from 'src/models/FileNode';
import { SelectNode } from 'src/models/SelectNode';
import { OpsService } from 'src/services/Ops.service';

const SERVER_BASE_URL = `http://localhost:8080`;

@Component({
  selector: 'app-flowchart',
  templateUrl: './flowchart.component.html',
  styleUrls: ['./flowchart.component.css'],
})
export class FlowchartComponent implements OnInit {

  nodes: Node[] = [];
  hierarchialGraph = {nodes: [], links: []}


  @Input() addedNodeName: string;
  @Input() addedFileNode: MFile;

  curve = shape.curveBundle.beta(1);

  nodeSourceId: number;
  nodeTargetId: number;

  constructor(private router: Router, 
              private nodesService: NodesService, 
              private filesService: FilesService,
              private opService: OpsService) { }

  customColors = [
    {
      name: "orange",
      value: "orange"
    }   
  ];

  ngOnInit() {
    this.nodes = this.nodesService.nodes
    this.hierarchialGraph = this.nodesService.hierarchialGraph;
    if(this.nodes.length != 0)
      this.showGraph()
  }

  ngOnChanges(){
    if(this.addedNodeName != null){
      this.nodesService.addNewNode(this.addedNodeName)
    }
    if(this.addedFileNode != null){
      this.nodesService.addNewNode('File',this.addedFileNode)
    }
    
    this.flush()
    this.showGraph()
    
  }

  onDblClickNode(node :any){
    const nodeId = node.id
    const nodeType = node.label.toLocaleLowerCase()
    const routeUrl = '/'+nodeType+'node/'+nodeId
    this.router.navigate([routeUrl])
  }

  /* /!\ NOT CLEAN TO REFACTOR  */
  onRightClick(node: any){
    if(this.nodeSourceId == null){
      this.nodeSourceId = node.id
    }
    else{
      this.nodeTargetId = node.id
      this.opService.setGraphLink(this.nodeSourceId,this.nodeTargetId)
      this.hierarchialGraph.links.push(
        {
          source: this.nodeSourceId,
          target: this.nodeTargetId
        }
      )
      this.nodesService.hierarchialGraph;
      this.opService.setGraphLink(this.nodeSourceId,this.nodeTargetId)
      console.log(this.opService.graph.nodes())
      //this.linking()
      this.showGraph()
      this.flush()
    }
    return false

  }

  showGraph() {

    this.hierarchialGraph.nodes = this.nodes.map(function(node){
      return { id: ''+node.id , label: node.name };
    }) 

  }

  flush(){
    this.addedFileNode = null;
    this.addedNodeName = null;
  }

  linking(){
    const nodeSource = this.nodesService.getNode(this.nodeSourceId)
    const nodeTarget = this.nodesService.getNode(this.nodeTargetId)
    if(nodeTarget.name.match('Select') && nodeSource.name.match('File')){
      /*this.filesService.getColumns(`${SERVER_BASE_URL}/files/`,(<FileNode>nodeSource).file)
        .subscribe(resp=>{
          (<SelectNode>nodeTarget).columns = resp;
          console.log('columns done :',resp);
        },err=>{
          console.log('columns failed',err);
        })*/
        //<SelectNode>nodeTarget).columns = (<FileNode>nodeSource).file.co
    }
  }

}

  

