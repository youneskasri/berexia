import { Component, OnInit, Input } from '@angular/core';
import { OpsService } from 'src/services/Ops.service';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.css']
})
export class PreviewComponent implements OnInit {


  columnDefs = [
   
  ];

  rowData = [
   
  ];

  res:any;

  constructor(private opService:OpsService) { }

  ngOnInit() {
    this.res = this.opService.res
    console.log(this.res)
    const rowData = this.res
    this.rowData = this.res
    const keys = Object.keys(rowData[0])
    for(let key of keys){
      this.columnDefs.push( { headerName: key, field: key} )
    }
  }

}
