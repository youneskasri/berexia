export class MFile {

    public table_name:string;
    public columns:string[];

    constructor(public name:string, public content:string){}
}