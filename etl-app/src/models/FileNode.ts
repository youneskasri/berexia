import { Node } from "./Node";
import { MFile } from "./MFile";

export class FileNode extends Node{

    constructor(public id:number, public name:String, public file:MFile) {
        super(id,name)
     }

     exec(){
        return this.file.table_name;
     }
}