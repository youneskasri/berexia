import { Node } from "./Node";

export class Operation{

    constructor(public sourceNode:Node, public targetNode:Node) { }

}