import { Node } from "./Node";
import { map } from 'rxjs/operators';
import { Http } from "@angular/http";



export class SelectNode extends Node{

    constructor(public id:number, public name:String) {
        super(id,name)
    }

    public input: any;

    private _operations: string[] = [];
    private _columns: string[] = [];

    /*mo2a9atan*/ 
    public allColumns: string [] = [];

    get columns() : string[] {
        return this._columns;
    }

    set columns(columns:string[]) {
        this._columns = columns;
    }

    get operations() : string[] {
        return this._operations;
    }

    addOperation(operation: string){
        this._operations.push(operation);
    }

    /*CONFIGURATION */ 
    exec() {
  
    }






}