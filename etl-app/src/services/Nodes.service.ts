import { SelectNode } from "src/models/SelectNode";
import { Node } from "src/models/Node";
import { CombineNode } from "src/models/CombineNode";
import { GroupByNode } from "src/models/GroupByNode";
import { FileNode } from "src/models/FileNode";
import { MFile } from "src/models/MFile";

export class NodesService{

    nodes: Node[] = [];
    hierarchialGraph = {nodes: [], links: []}


    addNewNode(nodeType:string, file?:MFile){
        const nodeId = this.nodes.length;
        if(nodeType.match('Select'))
          this.nodes.push(new SelectNode(nodeId,nodeType))
        else if(nodeType.match('Combine'))
          this.nodes.push(new CombineNode(nodeId,nodeType))
        else if(nodeType.match('GroupBy'))
          this.nodes.push(new GroupByNode(nodeId,nodeType))
        else if(nodeType.match('File'))
          this.nodes.push(new FileNode(nodeId,nodeType,file))
    }


    getNode(id:number){
      return this.nodes.find(node => node.id == id)
    }

    addNewLink(nodeSourceId, nodeTargetId){
      this.hierarchialGraph.links.push(
        {
          source: nodeSourceId,
          target: nodeTargetId
        }
      )
    }
}