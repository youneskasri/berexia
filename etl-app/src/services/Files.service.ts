import { FileNode } from "src/models/FileNode";
import { Http } from "@angular/http";
import { MFile } from "src/models/MFile";
import { map } from 'rxjs/operators';
import { Injectable } from "@angular/core";

@Injectable()
export class FilesService{

    constructor(private http:Http){ }

    files: FileNode[] = [];

    addFileNode(file: FileNode){
        this.files.push(file);
    }

    uploadFileToServer(route:string,file:MFile){
        return this.http.post(route, file)
            .pipe(map(resp=>resp.json()))
    }

    getColumns(route:string ,file:MFile){
        return this.http.get(route+file.table_name+'/columns')
            .pipe(map(resp=>resp.json()))
    }


}