import {Graph} from "@dagrejs/graphlib"
import { Injectable } from "@angular/core";
import { NodesService } from "./Nodes.service";
import { SelectNode } from "src/models/SelectNode";
import { Http } from "@angular/http";
import { map } from 'rxjs/operators';

import { SERVER_BASE_URL } from "../params";

@Injectable()
export class OpsService{

    res:any;

    constructor(private nodeService:NodesService, private http:Http){}

    graph = new Graph()


    setGraphNode(nodeId:number){
        this.graph.setNode(nodeId,nodeId)
    }

    setGraphLink(nodeSourceId, nodeTargetId){
        this.graph.setEdge(nodeSourceId,nodeTargetId)
    }

    execute(){
        const g = this.graph;
        const node0 = g.node(0) || g.nodes()[0]
        const s = g.successors(0)
        const root = this.nodeService.getNode(node0)

        let input  = root.exec()
        let res;
        for(const ss of s){
            const node = this.nodeService.getNode(ss)
            node.input = input;
            if(node.name.match('Select')){
                this.selectExec(<SelectNode>node)
                    .subscribe(resp=>{
                        alert('Processing done !')
                        this.res= resp;
                    },err=>{
                        console.log(err)
                    })
            }

        }

    }

    selectExec(select:SelectNode){
        return this.http.post(`${SERVER_BASE_URL}/files/${select.input}/select`,
        {op:'Select', tableName: select.input ,columns: select.columns, operations : select.operations})
            .pipe(map(resp=>resp.json()))
    }
}