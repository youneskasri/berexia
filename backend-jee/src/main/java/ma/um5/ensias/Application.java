package ma.um5.ensias;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Iterator;

import  org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.Getter;
import ma.um5.ensias.services.MyCsvParser;

@SpringBootApplication
public class Application {
	
	
		
	public static void main(String[] args) throws IOException {
		SpringApplication.run(Application.class, args);
		
		/* Test CSVPArser */
		String csvContent = "Column 1;Column 2;Column 3\r\nValue 1;Value 2;Value 3\r\n";
		ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64.encodeBase64(csvContent.getBytes()));
		Iterable<CSVRecord> it = new MyCsvParser(inputStream).getRecords();		
		it.forEach(record -> printRecord(record));
	
		/* Test toSQLCase */
		System.out.println(toSqlCase("c) ng new FirstApp\n" + 
				"\n" + 
				"ng servce : Pour transpiler TS en Js\n" + 
				"localhost:4200"));
	
	}


	private static void printRecord(CSVRecord csvRecord) {
		long recordNumber = csvRecord.getRecordNumber();
		System.out.println("Record number = " + recordNumber);
		csvRecord.forEach(value -> System.out.println(value));
	}
	
	
 
	
	public static String toSqlCase(String input) {
		StringBuilder output = new StringBuilder();
		
		input.chars().forEach(ch -> {
			if (Character.isSpaceChar(ch)) {
				output.append('_');
			} else if (Character.isAlphabetic(ch)) {
				output.append((char)Character.toUpperCase(ch));
			} else if (Character.isDigit(ch)) {
				output.append((char)ch);
			}
		});;
		
		return output.toString();
	}
		
	
}





