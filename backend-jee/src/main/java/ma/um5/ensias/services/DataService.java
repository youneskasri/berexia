package ma.um5.ensias.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import lombok.Getter;

@Service
public class DataService {
	
	public static HashMap<String, List<String>> mapTableName_withColumns = new HashMap<>();
	private static final CSVFormat FORMAT_EXCEL_FR = CSVFormat.EXCEL.withDelimiter(';');
	
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	public Object createTableAndInsertData(InputStream is, String TABLE_NAME, final List<String> columns)
			throws IOException {
		MyCsvParser myCsvParser = new MyCsvParser(is);
		
		Iterable<CSVRecord> records = myCsvParser.getRecords();
		records.forEach(record -> {
			if (record.getRecordNumber() == 1) {
				record.forEach(value -> columns.add(toSqlCase(value)));
				createTableInDatabase(TABLE_NAME, columns);
				return;
			} 
			saveToDatabase(TABLE_NAME, columns, record);	
		});
		
		mapTableName_withColumns.put(TABLE_NAME, columns);
		
		return new Object() {
			private @Getter String tableName = TABLE_NAME;
			private @Getter List<String> columnsList = columns;
		};
	}
		
	/* executes a Query */
	private void createTableInDatabase(String TABLE_NAME, List<String> columns) {
		jdbcTemplate.execute("DROP TABLE "+ TABLE_NAME +" IF EXISTS");
		String createTableQuery = buildCreateTableQuery(columns, TABLE_NAME);
		System.out.println(createTableQuery);
        jdbcTemplate.execute(createTableQuery);
	}
	
	private String buildCreateTableQuery(List<String> columns, String TABLE_NAME) {
		StringBuilder createTableQuery = new StringBuilder("CREATE TABLE ");
		createTableQuery.append(TABLE_NAME);
		appendColumnsListWithParenthesesAndType(columns, createTableQuery);
		return createTableQuery.toString();
	}

	/* (col1 TEXT, col2 TEXT, col3 TEXT) */
	private StringBuilder appendColumnsListWithParenthesesAndType(List<String> columns, StringBuilder queryBuilder) {
		return appendColumnsList(columns, queryBuilder, true, true);
	}
	
	/* (col1, col2, col3) */
	private StringBuilder appendColumnsListWithParentheses(List<String> columns, StringBuilder queryBuilder) {
		return appendColumnsList(columns, queryBuilder, true, false);
	}
	
	/* col1, col2, col3 */ // for SELECT
	private StringBuilder appendColumnsList(List<String> columns, StringBuilder queryBuilder) {
		return appendColumnsList(columns, queryBuilder, false, false);
	};
	
	/* AutoConvert columns name to SQL_CASE */
	private StringBuilder appendColumnsList(List<String> columns, StringBuilder queryBuilder, Boolean addParentheses, Boolean addTypes) {
		char PO = '(', PF = ')';
		if (!addParentheses) {
			PO = ' '; PF = ' ';
		}		
		queryBuilder.append(PO);			
		for (int i =0, n = columns.size(); i <n ; i++) {
			queryBuilder.append(toSqlCase(columns.get(i)));
			if (addTypes) queryBuilder.append(" TEXT");
			queryBuilder.append(i < n-1 ? ',' : PF);
		}
		return queryBuilder.append(' ');
	}
	
	/* INSERT INTO .... */
	private void saveToDatabase(String TABLE_NAME, List<String> columns, CSVRecord record) {
		StringBuilder insertQueryBuilder = new StringBuilder("INSERT INTO ")
				.append(TABLE_NAME);
		
	//	System.out.println("173) insetQuery = " + insertQueryBuilder.toString());
		appendColumnsListWithParentheses(columns, insertQueryBuilder);
		appendValuesList(record, insertQueryBuilder);
		String insertQuery = insertQueryBuilder.toString();
		//System.out.println(insertQuery);
		jdbcTemplate.update(insertQuery);
	}
	
	/* VALUES('val1','val2'...) */
	private StringBuilder appendValuesList(CSVRecord record, StringBuilder queryBuilder) {
		queryBuilder.append(" VALUES (");
		record.forEach(value -> queryBuilder.append("'"+value+"'").append(','));			
		
		int lastIndex = queryBuilder.lastIndexOf(",");
		queryBuilder.replace(lastIndex, lastIndex+1, ")");
		
		return queryBuilder.append(' ');
	}
	
	
	public static String toSqlCase(String input) {
		StringBuilder output = new StringBuilder();
		
		input.chars().forEach(ch -> {
			if (Character.isSpaceChar(ch) || (ch == '_')) {
				output.append('_');
			} else if (Character.isAlphabetic(ch)) {
				output.append((char)Character.toUpperCase(ch));
			} else if (Character.isDigit(ch)) {
				output.append((char)ch);
			}
		});;
		
		return output.toString();
	}
				
	public List<Map<String, Object>> selectAll(String TABLE_NAME, List<String> columns) {
		StringBuilder selectQueryBuilder = new StringBuilder("SELECT ");
			appendColumnsList(columns, selectQueryBuilder)
			.append(" FROM ").append(TABLE_NAME);
						
		return jdbcTemplate.queryForList(selectQueryBuilder.toString());
	}
	
	public List<Map<String, Object>> selectColumnsWhere(String TABLE_NAME, List<String> columns,
			List<String> operations) {
		StringBuilder selectQueryBuilder = new StringBuilder("SELECT ");
			appendColumnsList(columns, selectQueryBuilder)
			.append(" FROM ").append(TABLE_NAME).append(' ');
			
		if (operations.size()>0)
			operations.forEach(op -> selectQueryBuilder.append(" "+op+" "));
			
						
		return jdbcTemplate.queryForList(selectQueryBuilder.toString());
	}
	
}
