package ma.um5.ensias.services;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import lombok.Getter;

public class MyCsvParser {
	
	private @Getter Base64InputStream decoderStream;
	private @Getter Reader reader;
	private @Getter Iterable<CSVRecord> records;
	
	public MyCsvParser(InputStream inputStream) throws IOException {
		this.decoderStream = new MyCustomBase64InputStream(inputStream, false);
		this.reader = new InputStreamReader(decoderStream, "utf-8");
		this.records = new CSVParser(reader, FORMAT_EXCEL_FR);
 
	}
	
	private static final CSVFormat FORMAT_EXCEL_FR = CSVFormat.EXCEL.withDelimiter(';');
}


class MyCustomBase64InputStream extends Base64InputStream {

	private InputStream in;
	
	public MyCustomBase64InputStream(InputStream in, boolean doEncode) {
		super(in, doEncode);
		this.in = in;
	}

	@Override
	public int read(byte[] b, int offset, int len) throws IOException {
		char c = (char)b[0];
		if (c == '"') return EOF;
		return super.read(b, offset, len);
	}
	
	public static final int EOF = -1;		
}