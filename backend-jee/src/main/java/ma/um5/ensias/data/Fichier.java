package ma.um5.ensias.data;

import lombok.Data;
 
@Data
public class Fichier {
	private String name;
	private String content;
}
