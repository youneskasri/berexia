package ma.um5.ensias.data;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class SelectExec {
	private String op;
	private String tableName;
	private List<String> columns = new ArrayList<>(); 
	private List<String> operations = new ArrayList<>(); ;
}