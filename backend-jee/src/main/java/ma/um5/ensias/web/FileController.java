package ma.um5.ensias.web;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.um5.ensias.data.SelectExec;
import ma.um5.ensias.services.DataService;

@RestController
@RequestMapping("/files")
@CrossOrigin(origins = "http://localhost:4200")
public class FileController {
	
	@Autowired
	private DataService dataService;
	
	@PostMapping
	public Object addCsvFile(HttpServletRequest request /*, @RequestBody Fichier fichier*/) throws IOException {
			
		InputStream is = request.getInputStream();
		String filename = getFilename(is);
		String TABLE_NAME = DataService.toSqlCase(filename);
		final List<String> columns = new ArrayList<String>(); 
		skipUntil(':', is);
		skipUntil(',', is); // base64,
		return dataService.createTableAndInsertData(is, TABLE_NAME, columns);
	}
	
	@GetMapping("/{TABLE_NAME}/columns")
	public List<String> getColumnsList(@PathVariable String TABLE_NAME) {
		return DataService.mapTableName_withColumns.get(TABLE_NAME);
	}

	@GetMapping("/{TABLE_NAME}/select")
	public List<Map<String, Object>> selectSomeValues(@PathVariable String TABLE_NAME, 
			HttpServletResponse response) throws IOException {
		List<String> columns = DataService.mapTableName_withColumns.get(TABLE_NAME);		
		return dataService.selectAll(TABLE_NAME, columns);
	}

	@PostMapping("/{TABLE_NAME}/select")
	public List<Map<String, Object>> select(@PathVariable String TABLE_NAME, 
			@RequestBody SelectExec body) throws IOException {
		
		List<String> columns = body.getColumns();
		List<String> operations = body.getOperations();
		System.out.println(body);
		return dataService.selectColumnsWhere(TABLE_NAME, columns, operations);
	}

	
	private String getFilename(InputStream is) throws IOException {
		char c; 
		
		skipUntil(':', is);
		is.skip(2);
		
		String filename = new String();
		while ( (c = (char) is.read()) != '"' ) {
			filename += c;
		}
		
		return filename;
	}
 
	private void skipUntil(char stop, InputStream is) throws IOException {
		while ( ((char) is.read()) != stop );
	}

}


